package com.upplication.training.solid;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;


/**
 * Find your personal data in the system and do some operations:
 * 1. edit => Change the age
 * 2. remove => Remove from the memory DB
 * 3. view => Show the name and the age
 */
public class Main {

    public static void main(String ... args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Find a user by name:\nTry with: (Maria, Luis, Marta, Rick or Morty)\n");

        String username = scanner.next();

        if (username.length() < 3) {
            System.out.print("Your name must be longer than 3 characters\n");
            return;
        }

        MemoryDB memoryDB = new MemoryDB("hsqldb:mem://localhost/connect");

        Optional<People> peopleOpt = memoryDB.peoples.stream().filter((s) -> s.name.equals(username)).findFirst();
        if (!peopleOpt.isPresent()) {
            System.out.print(String.format("People with username %s not found, try again!", username));
        } else {
            People people = peopleOpt.get();
            System.out.print(String.format("People with username %s found!, what do you want to do?\n [1] Show info\n [2] Edit info\n [3] Remove\n", username));
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    System.out.println(String.format("User %s age is %d", username, people.age));
                    break;
                case 2:
                    System.out.println(String.format("Your actual age is: %d Enter your new age: ", people.age));
                    int a = scanner.nextInt();
                    while (a < 18) {
                        System.out.print("You must be over 18 years old. Try again....");
                        a = scanner.nextInt();
                    }
                    people.age = a;

                    memoryDB.peoples.stream().filter((p) -> p.name.equals(people.name)).map(p -> {
                        p.name = people.name;
                        p.age = people.age;
                        return p;
                    });

                    System.out.print(String.format("Your user: %s has been saved successfully, your new age is: %d", people.name, people.age));
                    break;
                case 3:
                    System.out.println(String.format("You are going to delete the user: %s Are you sure?\n [1] Yes\n [2] No\n", people.name));
                    int s = scanner.nextInt();
                    if (s == 1) {
                        memoryDB.peoples =  memoryDB.peoples.stream().filter((p) -> !p.name.equals(people.name)).collect(Collectors.toList());
                        System.out.print(String.format("Your user: %s ha been deleted!\n", people.name));
                    } else {
                        System.out.print("Deleted operation cancelled... bye!\n");
                    }
                    break;
                default:
                    System.out.print(String.format("Your option %d is not valid. Enter a valid option...", option));
            }

        }
    }


    public static class MemoryDB {

        public MemoryDB(String connection) {
            assert connection != null && connection.length() > 10;
        }

        public List<People> peoples = new ArrayList<People>() {{
            add(new People("Maria", 99));
            add(new People("Luis", 98));
            add(new People("Marta", 97));
            add(new People("Rick", 96));
            add(new People("Morty", 95));
        }};
    }

    public static class People {
        public String name;
        public int age;

        public People(String s, int i) {
            name = s;
            age = i;
        }
    }
}
